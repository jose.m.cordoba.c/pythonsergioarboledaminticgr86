# Entradas Distancia y el tiempo recorrido
# Proceso: Distancia / tiempo
# Salida: Velocidad promedio

distancia = int(
    input("Por favor ingrese la distancia recorrida en kilometros "))
tiempo = int(input("Por favor ingrese el tiempo del viaje en horas "))
velocidad = distancia / tiempo

print(
    f'Para la distancia de {distancia}km en un tiempo de {tiempo}h la velocidad promedio es de {velocidad}km/h')
