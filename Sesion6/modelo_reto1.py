"""Plantilla para resolver el reto # 1
    """
salario_base, horas_extra, hay_bono = input().split()
salario_base = float(salario_base)


def calcular_valor_bruto(salario_base, horas_extra, hay_bono) -> float:
    """[summary]

    >>> calcular_valor_bruto(1, 1, 1)
    1.1

    Returns:
        float: [description]
    """


def calcular_salario_neto(salario_bruto) -> float:
    """[summary]

    >>> calcular_salario_neto(1.1)
    0.9

    Args:
        salario_bruto ([type]): [description]

    Returns:
        float: [description]
    """


valor_bruto = calcular_valor_bruto(salario_base, horas_extra, hay_bono)
valor_neto = calcular_salario_neto(valor_bruto)


print(valor_bruto, valor_neto)
