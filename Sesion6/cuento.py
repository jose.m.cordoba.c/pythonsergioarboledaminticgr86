"""Programa que escribe un cuento personalizado
    """

nombre, profesion, arma, monstruo = input('''Digita el nombre,
la profesion, 
el arma,
el monstruo de la historia
''').split(',')

print(f'''Esta es la historia de {nombre},
Quien es un valiente {profesion},
que con su {arma},
derrotó a un {monstruo}
''')
