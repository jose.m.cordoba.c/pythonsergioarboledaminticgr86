"""Calcula el interes generado
    """

TASA_EFECTIVA = 0.15


def calcular_rendimiento_mensual(inversion) -> float:
    """Calcula el rendimiento generado en un mes

    >>> calcular_rendimiento_mensual(100)
    1.17

    >>> calcular_rendimiento_mensual(0)
    0.0

    >>> calcular_rendimiento_mensual(200)
    2.34

    Args:
        inversion ([type]): [description]

    Returns:
        float: [description]
    """
    tasa_mensual = obtener_tasa_mensual(TASA_EFECTIVA)
    rendimiento_mensual = inversion * tasa_mensual
    return rendimiento_mensual


def obtener_tasa_mensual(tasa_efectiva_anual) -> float:
    """Transforma una tasa efetiva anual en mensual

    >>> obtener_tasa_mensual(0.15)
    0.0117

    >>> obtener_tasa_mensual(0.30)
    0.0221

    Args:
        tasa_efectiva_anual ([type]): [description]

    Returns:
        float: [description]
    """

    return round((1 + tasa_efectiva_anual) ** (1 / 12) - 1, 4)
