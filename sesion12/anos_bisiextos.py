"""Muestra los años bisiextos en un rango excluyendo los multiplos de 10
    """


def es_multiplo(numero: int, multiplo: int) -> bool:
    """Determina si un numero es multiplo de otro

    >>> es_multiplo(9, 3)
    True

    >>> es_multiplo(7, 2)
    False

    Args:
        numero (int): [description]
        multiplo (int): [description]

    Returns:
        bool: [description]
    """
    return numero % multiplo == 0


def es_bisiexto(año: int) -> bool:
    """Determina si un año es bisiexto

    >>> es_bisiexto(10)
    False

    >>> es_bisiexto(2400)
    True

    >>> es_bisiexto(100)
    False

    >>> es_bisiexto(1996)
    True

    Args:
        ano (int): El año a valorar

    Returns:
        bool: True Si el año es bisiexto, False de lo contrario
    """
    return es_multiplo(año, 400) or (es_multiplo(año, 4) and not es_multiplo(año, 100))


desde, hasta = input('Ingrese el rango de años separado por espacio ').split()
desde = int(desde)
hasta = int(hasta)

for año in range(desde, hasta):
    if es_multiplo(año, 10) and es_bisiexto(año):
        print(f'El {año} es bisiexto y es multiplo de 10')
