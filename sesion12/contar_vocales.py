"""Ejemplos de conteo de vocales
"""


def es_vocal(letra) -> bool:
    """Determina si una letra es vocal

    >>> es_vocal('o')
    True

    >>> es_vocal('I')
    True

    >>> es_vocal('P')
    False

    >>> es_vocal('r')
    False

    >>> es_vocal('AE')
    False

    Args:
        letra (str len 1): una letra

    Returns:
        bool: True si es una vocal, False de lo contrario
    """
    return len(letra) == 1 and letra in 'AEIOUaeiou'


def es_consonante(letra) -> bool:
    """[summary]

    >>> es_consonante('o')
    False

    >>> es_consonante('I')
    False

    >>> es_consonante('P')
    True

    >>> es_consonante('r')
    True

    >>> es_vocal('pp')
    False

    Args:
        letra ([type]): [description]

    Returns:
        bool: [description]
    """
    return not es_vocal(letra)


def contar_vocales(texto) -> int:
    """ Cuenta la cantidad de vocales en un texto

    >>> contar_vocales('texto')
    2

    >>> contar_vocales('hola mundo')
    4

    >>> contar_vocales('')
    0

    Args:
        texto ([type]): [description]

    Returns:
        int: [description]
    """
    contador = 0
    for letra in texto:
        if es_vocal(letra):
            contador += 1
    return contador


frase = input('Ingresa una frase y te dire la cantidad de vocales ')
numero_vocales = contar_vocales(frase)
print(f"""En tu frase
{frase}
Hay {numero_vocales} vocales
""")
