"""Ejercicio de factorial con for
    """


def factorial(numero) -> int:
    """Calcula el factorial para un numero dado

    >>> factorial(5)
    120

    >>> factorial(0)
    1

    >>> factorial(4)
    24

    Args:
        numero (int): El limite del factorial

    Returns:
        int: [description]
    """
    acumulador = 1
    for n in range(1, numero + 1):
        acumulador *= n
    return acumulador
