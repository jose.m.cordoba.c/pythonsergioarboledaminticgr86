""" Ejercicios de ejmplo estructura for
    """

# Imprmir todos los numeros del 0 al 9
for numero in range(10):
    print(numero)


print('invertimos')
# Imprimir todos los numeros de 10, 0
for numero in range(10, 0, -1):
    print(numero)

# Imprimamos todos los numeros de 2 en dos
print('de dos en dos')
for n in range(0, 25, 2):
    print(n)


# Imprimir decreciendo desde 100 de 5 en 5
for x in range(100, -1, -5):
    print(x)
