"""Programa para leer numeros, sumar los negativos y promediar los positivos
    """

sumatoria_negativos = 0
promedio_positivos = 0
contador_positivos = 0

for i in range(6):
    entero = int(input('Por favor ingrese un numero entero '))
    if entero < 0:
        sumatoria_negativos += entero
    else:
        promedio_positivos += entero
        contador_positivos += 1

print(f'La sumatoria de numeros negativos es {sumatoria_negativos}')

if contador_positivos > 0:
    promedio_positivos /= contador_positivos
    print(f'El promedio de los numeros positivos es {promedio_positivos}')
else:
    print('No hubo numeros positivos')
