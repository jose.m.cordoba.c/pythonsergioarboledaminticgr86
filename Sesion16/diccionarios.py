"""Ejemplos con diccionarios
"""

mi_diccionario = {
    'saludo': "Hola",
    'nombre': 'Jose',
    'hobby': 'Tocar el bajo'
}

print(f"""{mi_diccionario['saludo']}, {mi_diccionario['nombre']}, que bueno que te guste {mi_diccionario['hobby']}
""")

# Agregar un elemento
mi_diccionario['edad'] = 34

print(f"'{mi_diccionario['nombre']}, tienes {mi_diccionario['edad']}")
