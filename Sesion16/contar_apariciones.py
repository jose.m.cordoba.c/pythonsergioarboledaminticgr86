"""Programa para contar las apariciones de las letras de un texto y retornarlas en un diccionario
    """


def contar_apariciones(texto: str) -> dict:
    """

    >>> contar_apariciones('Hola Mundo')
    {'H': 1, 'o': 2, 'l': 1, 'a': 1, ' ': 1, 'M': 1, 'u': 1, 'n': 1, 'd': 1}

    >>> contar_apariciones('anitalavalatina')
    {'a': 6, 'n': 2, 'i': 2, 't': 2, 'l': 2, 'v': 1}

    """
    resultado = {}
    for letra in texto:
        if letra in resultado:
            resultado[letra] += 1
        else:
            resultado[letra] = 1
    return resultado


print(contar_apariciones('anitalavalatina'))
