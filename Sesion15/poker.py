"""Ejercicio de representacion de cartas de poker
    """

# representemos una carta de poker como una tupla de len 2
# Ej ('🧡', 'A') ('🍀', '10') ('♠️', '2') ('💎', 'K')


def es_poker(lista_cartas: list) -> bool:
    """Determina si en las 5 cartas hay un poker

    >>> es_poker([('🧡', 'K'), ('🍀', 'K'), ('♠️', 'K'), ('💎', 'K'), ('💎', 'J')])
    True

    >>> es_poker([('🧡', '10'), ('🍀', 'K'), ('♠️', 'K'), ('💎', 'K'), ('💎', 'J')])
    False

    Args:
        lista_cartas (list): Lista de len = 5 con tuplas que representan las cartas

    Returns:
        bool: True si hay 4 cartas con el mismo valor, false de lo contrario
    """
