# Las tuplas con colecciones inmutables
mi_tupla = ("Hola mundo", "buenas", "Que mas")
print(type(mi_tupla))
print(mi_tupla)

# No podemos modificar elementos
## mi_tupla[0] = "Hola mundo cruel"

# Podemos acceder a los elementos de una tupla
print(mi_tupla[1])  # Imprimimos el segundo elemento
