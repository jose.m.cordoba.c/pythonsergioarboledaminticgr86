"""Ejemplos con listas
    """

# Lista homegenea
cultura_chupistica = ["absolut", "Chivas regal",
                      "Jonny Walker", "Tequimon", "Eduardo tercero", "Chamber", "Moscato pasito", "Jose Cuervo"]

# Lista heterogenea
lista = [100, "Hola mundo", False, 3.14]

# Imprimiendo listas
print(cultura_chupistica)

print(lista)

# Acceder a un elemento (Contamos los elementos desde 0)
print(cultura_chupistica[2])

print(lista[-1])

# Concatenar listas
lista_concatenada = cultura_chupistica + lista
print(lista_concatenada)

# insertar elemento
lista_concatenada.insert(0, "Coco chevere")
print(lista_concatenada)

# Contar
jonny = lista_concatenada.count("Jonny Walker")
print(jonny)

# Buscar
jonny = lista_concatenada.index("Jonny Walker")
print(jonny)

# Convertir a lista
hola = list("Hola mundo")
print(type(hola))
print(hola)
print(hola[3])

palabras = "hola mundo".split()
print(palabras)

# Sacar uno por uno todos los elementos usando pop()
while hola:
    print(hola.pop())
