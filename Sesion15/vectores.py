"""Funciones, para vectores
    """


def producto_punto(v1: list, v2: list) -> float:
    """Calcula el producto punto de dos vectores

    >>> producto_punto([1, 1, 1], [0, 0, 0])
    0

    >>> producto_punto([3, 2, 3], [-1, 0, 1])
    0

    >>> producto_punto([1, 2, 3], [1, 2, 3])
    14

    Args:
        v1 (list): [description]
        v2 (list): [description]

    Returns:
        float: [description]
    """
    acumulador = 0
    for i in range(len(v1)):
        acumulador += v1[i] * v2[i]
    return acumulador


def son_ortogonales(v1: list, v2: list) -> bool:
    """Determina si dos vectores son ortogonales

    >>> son_ortogonales([1, 1, 1], [0, 0, 0])
    True

    >>> son_ortogonales([3, 2, 3], [-1, 0, 1])
    True

    >>> son_ortogonales([1, 2, 3], [1, 2, 3])
    False

    Args:
        v1 (list): [description]
        v2 (list): [description]

    Returns:
        bool: [description]
    """
    return producto_punto(v1, v2) == 0
