"""Ejemplos con strings

"""

cadena = 'Esta es una cadena \' '
print(cadena)

# Tambien puedo fugar saltos de linea
cadena = 'Esto lleva un \n salto de linea '
print(cadena)

# Tambien puedo fugar el \ y tab
cadena = 'Esto lleva un \\ y un \t tab'
print(cadena)

# Para saber la longitud
print(f'mi cadena mide {len(cadena)}')

# Obtener una letra en la cadena
print(cadena[9])

print(cadena[-1])

# Obtener un ´Slice¨ de una cadena
print(cadena[0:5])
print(cadena[-5:-1])
print(cadena[-5:])

# operaciones con cadenas
print(cadena[-3:] * 3)
print(cadena[-3:] + cadena[0:5])

# ver comparaciones
print('Esto' == 'Aquello')
print('Esto' >= 'Aquello')
print('Esto' <= 'Aquello')
print('Esto' != 'Aquello')

# Conversiones
print('esto es mayusculas'.upper())
print('ESTO ES MINUSCULAS'.lower())
print('esto es UN TITULO'.title())

cadena_sucia = '       hola mundo                           '
print(
    f'limpiamos lo de la izquierda "{cadena_sucia.lstrip()}" pero no lo de la derecha')
print(
    f'limpiamos lo de la derecha "{cadena_sucia.rstrip()}" pero no lo de la izquierda')
print(
    f'limpiamos la cadena "{cadena_sucia.strip()}"')

hola, mundo = cadena_sucia.strip().split()
print(hola, mundo)


print(cadena_sucia.replace('hola', 'Buen').strip())
