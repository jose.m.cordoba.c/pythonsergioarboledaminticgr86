"""Programa para contar palabras
    """


def contar_palabras(texto: str) -> int:
    """Cuenta cuantas palabras hay en un texto

    >>> contar_palabras('hola mundo')
    2

    >>> contar_palabras('')
    0

    >>> contar_palabras('La bella y graciosa moza marchose a lavar la ropa ')
    10

    >>> contar_palabras('racecar')
    1

    Args:
        texto (str): El texto a evaluar

    Returns:
        int: el conteo de palabras
    """
    return len(texto.split())
    # TODO hacerlo con for con fines académicos
