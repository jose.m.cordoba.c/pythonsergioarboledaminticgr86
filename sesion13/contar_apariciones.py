"""Cuenta cuantas veces aparece un caracter en una cadena
    """


def es_caracter(posible_caracter: str) -> bool:
    """Valida si un str es un caracter

    >>> es_caracter('A')
    True

    >>> es_caracter('ae')
    False

    >>> es_caracter(' ')
    False

    Args:
        posible_caracter (str): el candidato a caracter

    Returns:
        bool: True si es un caracter, False de lo contrario
    """
    # FIXME Validar si hay una funcion de str para esto
    return len(posible_caracter) == 1 and posible_caracter.isalnum()
    # return len(posible_caracter) == 1 and posible_caracter.lower() in 'abcdefghijklmnopqrstuvwxyz'


def contar_apariciones(texto: str, caracter: str) -> int:
    """Cuenta cuantas apariciones hay del caracter en el texto

    >>> contar_apariciones('hola mundo', 'o')
    2

    >>> contar_apariciones('hola mundo', 'z')
    0

    >>> contar_apariciones('hola mundo', 'op')
    '"op" No es un caracter valido'

    Args:
        texto (str): [description]
        caracter (str): [description]

    Returns:
        int: [description]
    """
    if not es_caracter(caracter):
        return f'"{caracter}" No es un caracter valido'
    contador = 0
    for letra in texto:
        if letra == caracter:
            contador += 1
    return contador
