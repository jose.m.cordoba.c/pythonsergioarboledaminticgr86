"""Programa que reemplaza la primer aparicion de un caracter 
en una cadena por otro
"""

from contar_apariciones import es_caracter


def reemplazar_primer_aparicion(texto: str, caracter_a_buscar: str, caracter_de_reemplazo: str) -> str:
    """Reemplaza la primer aparicion del caracter a buscar por el caracter de reemplazo

    >>> reemplazar_primer_aparicion('hola mundo', 'hola', 'h')
    '"hola" no es un caracter valido'

    >>> reemplazar_primer_aparicion('hola mundo', 'h', 'mundo')
    '"mundo" no es un caracter valido'

    >>> reemplazar_primer_aparicion('hola mundo', 'm', 'M')
    'hola Mundo'

    >>> reemplazar_primer_aparicion('hola mundo', 'o', 'O')
    'hOla mundo'

    >>> reemplazar_primer_aparicion('hola mundo', 'h', 'H')
    'Hola mundo'

    >>> reemplazar_primer_aparicion('hola mundo z', 'z', 'Z')
    'hola mundo Z'


    Args:
        texto (str): [description]
        caracter_a_buscar (str): [description]
        caracter_de_reemplazo (str): [description]

    Returns:
        str: [description]
    """

    if not es_caracter(caracter_a_buscar):
        return f'"{caracter_a_buscar}" no es un caracter valido'
    if not es_caracter(caracter_de_reemplazo):
        return f'"{caracter_de_reemplazo}" no es un caracter valido'

    contador = 0
    for letra in texto:
        if letra == caracter_a_buscar:
            # Sacamos un slice de lo anterior agregamos nuestra letra y un slice de lo posterior
            return texto[:contador] + caracter_de_reemplazo + texto[contador + 1:]
        contador += 1
