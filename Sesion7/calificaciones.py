"""Programa para calcular la nota definitiva de un estudiante
"""

# Constantes
FACTOR_TALLER = 0.1
FACTOR_QUIZ = 0.15
FACTOR_FINAL = 0.2


def calcular_nota_definitiva(taller1,
                             taller2,
                             taller3,
                             quiz1,
                             quiz2,
                             trabajo,
                             sustentacion) -> float:
    """Calcula la nota definitiva de un estudiante

    >>> calcular_nota_definitiva(10, 10, 10, 10, 10, 10, 10)
    10.0

    >>> calcular_nota_definitiva(7, 5, 6, 10, 10, 10, 10)
    8.8

    >>> calcular_nota_definitiva(7, 5, 6, 10, 10, 10, 0)
    6.8


    Args:
        taller1 ([float]): [Representa el 10% de la nota final]
        taller2 ([float]): [Representa el 10% de la nota final]
        taller3 ([float]): [Representa el 10% de la nota final]
        quiz1 ([float]): [Representa el 15% de la nota final]
        quiz2 ([float]): [Representa el 15% de la nota final]
        trabajo ([float]): [Representa el 20% de la nota final]
        sustentacion ([float]): [Representa el 20% de la nota final]

    Returns:
        float: [description]
    """
    nota_definitiva = 0

    # Talleres
    nota_talleres = (taller1 + taller2 + taller3) * FACTOR_TALLER
    nota_definitiva += nota_talleres

    # Quizes
    nota_quizes = (quiz1 + quiz2) * FACTOR_QUIZ
    nota_definitiva += nota_quizes

    # Proyecto Final
    nota_proyecto_final = (trabajo + sustentacion) * FACTOR_FINAL
    nota_definitiva += nota_proyecto_final

    return nota_definitiva
