"""Programa que define una funcion que lanza un error"""


def no_quiero_a_homero(nombre: str) -> str:
    """ Saluda a todos menos a homero

    >>> no_quiero_a_homero('bart')
    'hay caramba'

    >>> no_quiero_a_homero('homero')
    ValueError: Este es el club de los no homeros

    Args:
        nombre (str): [description]

    Returns:
        str: [description]
    """
    if nombre == 'homero':
        raise ValueError('Este es el club de los no homeros')
    elif nombre == 'bart':
        return 'hay caramba'
    return f'hola {nombre}'
