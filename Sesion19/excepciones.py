try:
    cadena = 'hola ' + 10 + ' veces'
except TypeError:
    cadena = f'hola {10} veces'

print(cadena)

try:
    x = 10 / 0
except ZeroDivisionError:
    x = 'No dividiras por 0'

print(x)
