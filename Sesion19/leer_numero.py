"""Programa que valida si un numero fue ingresado de forma correcta"""

while True:
    try:
        numero = input('Ingrese un numero ')
        numero = int(numero)
        break
    except ValueError:
        print(f'{numero} no es un entero valido')
    except:
        print('Ocurrio un error generico')
print(f'Su numero es {numero}')
