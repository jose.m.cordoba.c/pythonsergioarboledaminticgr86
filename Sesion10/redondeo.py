"""Ejemplos de redondeo
    """

numero = 7.235
equivalente = 7.2

if equivalente == round(numero, 1):
    print(f'El redondeo funciona al pelo {round(numero, 1)}')

numero = 7.276
equivalente = 7.3

if equivalente == round(numero, 1):
    print(f'El redondeo funciona al pelo {round(numero, 1)}')

numero /= round(equivalente)

if equivalente == numero:
    print(f'El redondeo funciona al pelo {numero}')
else:
    print(f'Me descache en el redondeo {numero}')
