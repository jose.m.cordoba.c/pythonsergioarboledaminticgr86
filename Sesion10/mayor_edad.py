def es_mayor_de_edad(edad) -> str:
    """[summary]

    >>> es_mayor_de_edad(18)
    'Buenas adulto'

    >>> es_mayor_de_edad(5)
    'No puedes entrar'

    Args:
        edad ([type]): [description]

    Returns:
        str: [description]
    """
    if edad >= 18:
        return "Buenas adulto"
    else:
        return "No puedes entrar"
