"""Programa para obtener los numeros en la serie fibonacci
    """

final = int(input('En que numero finalizamos la serie la serie'))


def fibonacci(n):
    contador = 0
    fibonacci_actual = 1
    fibonacci_anterior = 0
    while contador < final:
        print(fibonacci_anterior)
        fibonacci_anterior, fibonacci_actual = fibonacci_actual, fibonacci_anterior + fibonacci_actual
        contador += 1


fibonacci(final)
