"""Este programa captura un numero menor que 10
    """

numero = float(input('Por favor ingresa un numero menor que 10 '))

while numero >= 10:
    print('El número debe ser menor a 10!! 😤')
    numero = float(input('Por favor ingresa un numero menor que 10 '))

print(f'Tu numero es {numero}')
