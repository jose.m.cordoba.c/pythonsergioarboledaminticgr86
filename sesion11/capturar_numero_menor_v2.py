"""Este programa captura un numero menor que 10
    """

numero = float(input('Por favor ingresa un numero entre 0 y 20 '))

while numero < 0 or numero > 20:
    print('El número debe estar entre 0 y 20 😤')
    numero = float(input('Por favor ingresa un numero  entre 0 y 20'))

print(f'Tu numero es {numero}')
