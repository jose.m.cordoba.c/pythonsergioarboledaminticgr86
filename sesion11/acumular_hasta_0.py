"""Programa que acumula numeros hasta leer un 0
    """

acumulador = 0

while True:
    numero = float(input('Ingresa un numero '))
    acumulador += numero
    if numero == 0:
        break
    else:
        print(f'Se sumo el {numero}, el total acumulado es {acumulador}')

print('Suerte mi 🐶')
