""" Programa para ver cuales son mis mejores estudiantes
"""

# Listado de los estudiantes
listado_estudiantes = {"jose": 10, "manuel": 2, "camilo": 7, "paula": 9, "viviana": 10,
                       "andrea": 8, "laura": 7, "maria": 6, "ana": 8, "carlos": 5, "luis": 6}

# Listado de los mejores
los_mejores = []

# Para cada estudiante en mi lista
for estudiante in listado_estudiantes:
    # si el resultado del estudiante es mayor o igual a 9
    resultado = listado_estudiantes[estudiante]
    if resultado >= 9:
        # Agrego el estudiante a los mejores
        los_mejores.append(estudiante)

    # Imprimo los mejores estudiantes
print(f'Los mejores estudiantes son {los_mejores}')
