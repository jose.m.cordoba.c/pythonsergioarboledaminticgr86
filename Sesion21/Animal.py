"""La clase animal representa todos los animales del reino natural
"""


class Animal:

    nombre = ''
    edad = ''
    sexo = ''

    def __init__(self, nombre, edad, sexo) -> None:
        self.nombre = nombre
        self.edad = edad
        self.sexo = sexo

    def __str__(self) -> str:
        return(f'El animal se llama {self.nombre} tiene una edad de {self.edad}, y es de sexo {self.sexo}')

    def sonar(self):
        pass
