"""Probaremos animales"""

from Animal import Animal

perro = Animal('firulais', '10 meses', 'macho')
gato = Animal('Karoo', '7 meses', 'macho')
loro = Animal('Tomas', '10 años', 'macho')

print(perro)
print(gato)
print(loro)
