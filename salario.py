"""Este es un programa que calcula el salario de un trabajador
"""

nombre = input('''Bienvenido

¿Cual es el nombre del trabajador? ''')
identificacion = input(f'¿Cual es la identificacion para {nombre}? ')
numero_horas_trabajadas = int(
    input(f'¿Cuantas horas trabajó {nombre} identificado con {identificacion}? '))
valor_hora = int(input('¿Cual es el valor de la hora trabajada? '))

salario = valor_hora * numero_horas_trabajadas

print(f'El salario de {nombre} es {salario}')
