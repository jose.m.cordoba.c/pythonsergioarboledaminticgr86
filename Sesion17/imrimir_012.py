def g(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n == 2:
        return 2
    else:
        return g(n - 3)


for i in range(43):
    print(g(i))
