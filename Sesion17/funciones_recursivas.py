"""Ejemplos de funciones recursivas
    """


def potencia(base: int, exponente: int) -> int:
    """

    >>> potencia(2, 6)
    64

    >>> potencia(4, 3)
    64

    >>> potencia(3, 4)
    81

    >>> potencia(123123, 0)
    1

    """
    if exponente == 0:
        return 1
    return base * potencia(base, exponente - 1)


def factorial(n: int) -> int:
    """

    >>> factorial(0)
    1

    >>> factorial(5)
    120

    """
    return 1 if n == 0 else n * factorial(n - 1)


def contar_digitos(n: int) -> int:
    """

    >>> contar_digitos(987654321)
    9

    >>> contar_digitos(0)
    1

    >>> contar_digitos(54321)
    5

    """
    if n < 10:
        return 1
    return 1 + contar_digitos(n // 10)


def mayor(lista: list) -> int:
    """

    >>> mayor([9, 2, 5, 7, 1])
    9

    >>> mayor([2, 5, 7, 1])
    7

    """
    if len(lista) == 1:
        return lista[0]
    lista_reducida = lista[1:]  # reducimos la lista en un elemento
    # Buscamos el mayor en la lista reducida
    mayor_de_la_derecha = mayor(lista_reducida)
    # comparamos el primer elemento con el mayor de la lista reducida
    if lista[0] > mayor_de_la_derecha:
        return lista[0]
    return mayor_de_la_derecha
